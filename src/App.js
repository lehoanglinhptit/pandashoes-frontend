import React, { useEffect, useState } from "react";
import Route from './route/router'
import HeaderTulam from "./components/common/HeaderTulam";
import Footer from './components/common/Footer'
import { useLocation } from 'react-router-dom';
export default () => {
    const [cond, setCond] = useState(window.location.pathname === "/Login" || window.location.pathname === "/Register");
    let location = useLocation();
    useEffect(() => {
        console.log("location: ", location)
        const cond = location.pathname === "/Login" || location.pathname === "/Register"
        cond ? setCond(true) : setCond(false)
    }, [location])
    // const cond2 = window.location.href.indexOf("/Login") !== -1
    return (
        <div>
            {!cond && <div><HeaderTulam /></div>}

            <div><Route /></div>
            <div><Footer /></div>
        </div>
    );

}

