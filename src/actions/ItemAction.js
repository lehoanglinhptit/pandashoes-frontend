import {ACTION_TYPE} from "../constants"
const {ITEM} = ACTION_TYPE
const getData ={
    request: (payload) => {
        return {
            type: ITEM.GET_LIST_REQUEST,
            payload
        }
    },
    success: (payload) => ({
        type: ITEM.GET_LIST_SUCCESS,
        payload
    }),
    failure: (payload) => ({
        type: ITEM.GET_LIST_FAILURE,
        payload
    })
}
const Login ={
    request: (payload) => {
        return {
            type: ITEM.LOGIN_REQUEST,
            payload
        }
    },
    success: (payload) => ({
        type: ITEM.LOGIN_SUCCESS,
        payload
    }),
    failure: (payload) => ({
        type: ITEM.LOGIN_FAIL,
        payload
    })
}
export{
    getData,Login
}