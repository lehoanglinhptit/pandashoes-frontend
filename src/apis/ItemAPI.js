import callAPI from './configAPI'
import {HTTP_METHOD,LIMIT} from '../constants'

const getAll = (textSearch)=> callAPI(`/items?q=${textSearch || ''}`,HTTP_METHOD.GET)
const getData = (textSearch,activePage)=>callAPI(`/items?q=${textSearch || ''}&_page=${activePage}&_limit=${LIMIT}`, HTTP_METHOD.GET)
const Login =(params)=>callAPI(`/login`,HTTP_METHOD.POST,params)
export {
    getAll,getData,Login
}