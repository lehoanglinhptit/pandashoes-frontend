import {HTTP_METHOD,HEADERS,DOMAIN} from '../constants'
 
export default (path, method, data)=> new Promise((resolve,reject)=>{
    const url = DOMAIN + path
    let options={
        method
    }
    if(method===HTTP_METHOD.POST|| method===HTTP_METHOD.PUT){
        options={
            method,
            headers:HEADERS,
            body: JSON.stringify(data)
        }
    }
    fetch(url,options)
    .then((response)=>resolve(response.json()))
    .catch((err)=>reject(err))

})