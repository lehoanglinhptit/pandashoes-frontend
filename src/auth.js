/**
 * This represents some generic auth provider API, like Firebase.
 */
 let token = localStorage.getItem('token')
 const fakeAuthProvider = {
    token : localStorage.getItem('token'),
    isAuthenticated:false,
    signin(callback) {
      if(fakeAuthProvider.token){
        this.isAuthenticated = true
      };
      setTimeout(callback, 100); // fake async
    },
    signout(callback) {
      fakeAuthProvider.isAuthenticated = false;
      setTimeout(callback, 100);
    },
  };
  
  export { fakeAuthProvider };
  