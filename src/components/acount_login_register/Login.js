
import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import "./account.css"
import { Login } from "../../actions/ItemAction";
import {
    useNavigate,
    useLocation
} from "react-router-dom";

const Login1 = (props) => {
    let navigate = useNavigate();
    let location = useLocation();
    let from = location.state?.from?.pathname || "/";
    const stateDefault = {
        username: "",
        password: "",
        hidePassword: true
    }
    const [state, setState] = useState(stateDefault);
    useEffect(()=>{
        props.data && navigate(from, { replace: true });
    }, [props.data])
    const handleState = (event) => {
        setState({
            ...state,
            [event.target.name]: event.target.value
        })
    }

    const handleClick = () => {
        props.Login({ userName: state.username, passWord: state.password })
    }

    const { hidePassword } = state
    return (
        <div className="LoginPage-SignUp">
            <div className="header-main">
                <h1>PandaShoes</h1>
                <div className="header-bottom">
                    <div className="header-right w3agile">

                        <div className="header-left-bottom agileinfo">

                            <input name="username" type="text" placeholder="username" onChange={handleState} />
                            <input name="password" type={hidePassword ? "password" : "text"} placeholder="password" onChange={handleState} />
                            {hidePassword ?
                                <button onClick={() => setState({ ...state, hidePassword: false })}>hien</button> :
                                <button onClick={() => setState({ ...state, hidePassword: true })}>an</button>}
                            <input type="submit" value="Login" onClick={handleClick} />

                            <div className="header-left-top">
                                <div className="sign-up"> <h2>or</h2> </div>

                            </div>
                            <div className="header-social wthree">
                                <a href="#" className="face"><h5>Facebook</h5></a>
                                <a href="#" className="twitt"><h5>Twitter</h5></a>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    );
}

const mapStateToProps = (state) => {
    const { data } = state.signin
    return {
        data
    }

}
const mapDispatchToProps = (dispatch) => ({
    Login: (payload) => {
        dispatch(Login.request(payload))
    },

})
export default connect(mapStateToProps, mapDispatchToProps)(Login1)