import React from 'react';
import { MDBFooter } from 'mdb-react-ui-kit';
import 'mdb-react-ui-kit/dist/css/mdb.min.css'

export default function App() {
  return (
    <MDBFooter bgColor='light' className='text-center text-lg-start text-muted'>


      <section className=''>
        <div className='container text-center text-md-start mt-5'>
          <div className='row mt-3'>
            <div className='col-md-3 col-lg-4 col-xl-3 mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>
                <i className='fas fa-gem me-3'></i>PandaShoes
              </h6>
              <p>
                Đơn vị chuyên cung cấp dòng sản phẩm giày high-retro tại Việt Nam.
              </p>
            </div>

            <div className='col-md-2 col-lg-2 col-xl-2 mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Products</h6>
              <p>
                <a href='#!' className='text-reset'>
                  Converse
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Vans
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Nike
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Adidas
                </a>
              </p>
            </div>

            <div className='col-md-3 col-lg-2 col-xl-2 mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Support</h6>
              <p>
                <a href='#!' className='text-reset'>
                  Pricing
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Settings
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Orders
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Help
                </a>
              </p>
            </div>

            <div className='col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Contact</h6>
              <p>
                <i className='fas fa-home me-3'></i> Hoang Mai,Ha Noi, Viet Nam
              </p>
              <p>
                <i className='fas fa-envelope me-3'></i>
                pandashoes@gmail.com
              </p>
              <p>
                <i className='fas fa-phone me-3'></i> + 84 234 567 88
              </p>
              <p>
                <i className='fas fa-print me-3'></i> + 84 234 567 89
              </p>
            </div>
          </div>
        </div>
      </section>

      <div className='text-center p-4' style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
        © 2022 Copyright:
        <a className='text-reset fw-bold' href='https://mdbootstrap.com/'>
          PandaShoes
        </a>
      </div>
    </MDBFooter>
  );
}