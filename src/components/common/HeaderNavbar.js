import React, { useState } from 'react';
import {
    MDBContainer, MDBNavbar, MDBNavbarBrand, MDBNavbarToggler, MDBIcon, MDBNavbarNav,
    MDBNavbarItem, MDBNavbarLink, MDBDropdown, MDBDropdownToggle,
    MDBDropdownMenu, MDBDropdownItem, MDBDropdownLink, MDBCollapse,
    MDBCol,
    MDBBtn
} from 'mdb-react-ui-kit';

import 'mdb-react-ui-kit/dist/css/mdb.min.css'
import './common.css'
import { NavLink } from 'react-router-dom';

export default function HeaderNavbar() {
    const [showBasic, setShowBasic] = useState(false);
    const [open, setOpen] = useState(false)
    const handleClick = () => { setOpen(true) }
    const handleClose = () => setOpen(false)
    const handleClickBTN = () => {
        console.log("1111111")
       localStorage.removeItem('token')
       window.location.reload(true)
       //window.location.href='/'   hai cách đều như nhau
    }

    return (
        <div>
            <MDBNavbar expand='lg' light bgColor='light'>
                <MDBContainer fluid>
                    <MDBNavbarBrand href='/'>PandaShoes</MDBNavbarBrand>

                    <MDBNavbarToggler
                        aria-controls='navbarSupportedContent'
                        aria-expanded='false'
                        aria-label='Toggle navigation'
                        onClick={() => setShowBasic(!showBasic)}
                    >
                        <MDBIcon icon='bars' fas />
                    </MDBNavbarToggler>

                    <MDBCollapse navbar show={showBasic}>
                        <MDBNavbarNav className='mr-auto mb-2 mb-lg-0'>
                            <MDBNavbarItem>
                                <MDBDropdown>
                                    <MDBDropdownToggle tag='a' className='nav-link'>
                                        Danh Mục Sản Phẩm
                                    </MDBDropdownToggle>
                                    <MDBDropdownMenu>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink >
                                                <NavLink to={`/ALLPRODUCTS`}>All PRODUCTS</NavLink>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink>
                                                <NavLink to={`/Vans`}>Vans</NavLink>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink><NavLink to={`/Converse`}>Converse</NavLink>
                                            </MDBDropdownLink>
                                        </MDBDropdownItem>
                                        <MDBDropdownItem>
                                            <MDBDropdownLink>Accessories</MDBDropdownLink>
                                        </MDBDropdownItem>
                                    </MDBDropdownMenu>
                                </MDBDropdown>
                            </MDBNavbarItem>
                            <MDBNavbarItem>
                                <MDBNavbarLink active aria-current='page' href='#'>
                                    Home
                                </MDBNavbarLink>
                            </MDBNavbarItem>
                            <MDBNavbarItem>
                                <MDBNavbarLink href='#'>Trending</MDBNavbarLink>
                            </MDBNavbarItem>
                            <MDBNavbarItem>
                                <MDBNavbarLink active aria-current='page' href='#'>
                                    Thông báo
                                </MDBNavbarLink>
                            </MDBNavbarItem>


                        </MDBNavbarNav>
                        <MDBCol md="5">
                            <form className="form-inline mt-4 mb-4">
                                <input className="form-control form-control-sm ml-3 w-75" type="text" placeholder="Search" aria-label="Search" />
                                {/* <button>Search</button> */}
                            </form>
                        </MDBCol>
                        <MDBCol>
                            {
                                localStorage.getItem('token') ? <div> <MDBNavbarLink active aria-current='page' href=''>
                                    <MDBIcon far icon="user-circle" />
                                </MDBNavbarLink>

                                </div>

                                    : <div style={{ display: "flex", paddingBottom: "2px" }}>
                                        <div style={{ width: "100px", fontSize: "17px", marginRight: "10px", cursor: "pointer" }} >
                                            <NavLink to={`/Login`}>Đăng Nhập</NavLink>
                                        </div>
                                        <div style={{ width: "100px", fontSize: "17px", cursor: "pointer" }}>
                                            <NavLink to={`/Register`}> Đăng Ký</NavLink>
                                        </div>
                                    </div>
                            }

                        </MDBCol>
                        {
                            localStorage.getItem('token') &&
                            <MDBCol>
                                <button onClick={handleClickBTN}>
                                    <MDBIcon icon="sign-out-alt" />
                                </button>
                            </MDBCol>
                        }
                        <MDBCol>


                            <MDBNavbarLink active aria-current='page' href='/Shopping-cart'>
                                <MDBIcon icon="shopping-cart" > </MDBIcon>
                            </MDBNavbarLink>

                        </MDBCol>

                    </MDBCollapse>
                </MDBContainer>

            </MDBNavbar >


        </div>
    );
}
