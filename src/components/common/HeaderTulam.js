import './common.css'
import React from 'react';

import HeaderNavbar from './HeaderNavbar';
import ActiveLastBreadcrumb from './BreadScrum';
export default class HeaderTulam extends React.Component {
    render() {
        return (
            <div >
                <header className="header">
                    <div className='grid'>
                        <HeaderNavbar/>
                        <ActiveLastBreadcrumb/>
                    </div>
                </header>
               
            </div>
        );
    }
}

