import React from "react";
import Carousel from "../slider/Carousel";
import ListHome from "../list_sp/listHome";


export default class Home extends React.Component {
    render() {
        return (
            <div>
            {/* <HeaderTulam/>   */}
              <Carousel/>
            <ListHome/>
            {/* <Footer/> */}
            </div>
        );
    }
}
