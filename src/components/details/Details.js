import React from 'react';
import './details.css';
import Colors from './Colors'
import DetailsThumb from './DetailsThumb';

class Details extends React.Component{

  state = {
    products: [
      {
        "_id": "1",
        "title": "Giày Converse Chuck Taylor All Star Patent Popoes",
        "src": [
            "https://bizweb.dktcdn.net/thumb/small/100/347/923/products/571620c-5.jpg?v=1638614517743",
            "https://bizweb.dktcdn.net/thumb/small/100/347/923/products/571620c-1.jpg?v=1638614524150",
            "https://bizweb.dktcdn.net/thumb/small/100/347/923/products/571620c-2.jpg?v=1638614527633",
            "https://bizweb.dktcdn.net/thumb/small/100/347/923/products/571620c-3.jpg?v=1638614530813"
          ],
        // "description": "UI/UX designing, html css tutorials",
        "content": "It started when we took scissors to our original High Top Chucks, turning them into low top basketball shoes. Then the sneaker escaped the court for the streets, cementing its status as an everyday icon. What’s next for the Low Top Chucks is up to you. They’re versatile, classic and entirely yours to wear any way you want, anywhere you want, anytime you want. We just make the shoe. You make the stories.",
        "price": 23,
        "colors":["red","black","crimson","teal"],
        "count": 1
      }
    ],
    index: 0
  };

  myRef = React.createRef();

  handleTab = index =>{
    this.setState({index: index})
    const images = this.myRef.current.children;
    for(let i=0; i<images.length; i++){
      images[i].className = images[i].className.replace("active", "");
    }
    images[index].className = "active";
  };

  componentDidMount(){
    const {index} = this.state;
    this.myRef.current.children[index].className = "active";
  }


  render(){
    const {products, index} = this.state;
    return(
      <div className="app">
        {
          products.map(item =>(
            <div className="details" key={item._id}>
              <div className="big-img">
                <img src={item.src[index]} alt=""/>
              </div>

              <div className="box">
                <div className="row">
                  <h2>{item.title}</h2>
                  <span>${item.price}</span>
                </div>
                <Colors colors={item.colors} />

                <p>{item.description}</p>
                <p>{item.content}</p>

                <DetailsThumb images={item.src} tab={this.handleTab} myRef={this.myRef} />
                <button className="cart">Add to cart</button>

              </div>
            </div>
          ))
        }
      </div>
    );
  };
}

export default Details;


