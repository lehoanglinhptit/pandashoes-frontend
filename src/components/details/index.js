import HeaderTulam from "../common/HeaderTulam";
import Footer from '../common/Footer'
import Details from "./Details";
import React from "react";
 export default class DeatailPage extends React.Component {
    render() {
        return (
            <div>
                <HeaderTulam/>
                <Details/>
                <Footer/>
            </div>
        );
    }
}
