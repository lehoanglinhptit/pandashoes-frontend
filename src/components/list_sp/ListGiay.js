import * as React from 'react';
import { styled } from '@mui/material/styles';
import { Box, Card, CardHeader, CardMedia, CardContent, Typography, Paper, Grid, Rating } from '@mui/material'
import '../common/common.css'
import Pagination from './Pagination';
import { getData } from '../../actions/ItemAction';
import { connect } from 'react-redux';
import {NavLink} from 'react-router-dom'

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

// 


class ListGiay extends React.Component {
  state={
    value: 2,
    
  }
  componentDidMount() {
    this.props.getData({ textSearch: '', activePage: 1 })

  }
  render() {
    const { isLoading, listData,code } = this.props
    console.log('listData', listData)
    let listVans = listData.filter((item,idx)=>
     item.code===code
    )
    
    console.log("LIST Vans",listVans)
    return (
      <div>
      
        {isLoading ?
          <img src='https://cdn.dribbble.com/users/902865/screenshots/4814970/loading-opaque.gif' />
          :
          <div className='product-containers'>
            <Box sx={{ flexGrow: 1 }}>
              <Grid container spacing={2}>
                {listVans.map((item, idx) => (
                  <Grid item xs={2} key={idx}>
                    <Item>
                      <Card sx={{ maxWidth: 350 }}>
                        <CardHeader
                          title={item.name}
                        />
                        <CardMedia
                          component="img"
                          height="200"
                          image={item.image}
                          alt="Paella dish"
                        />
                        <CardContent>
                          
                          <Typography variant="body2" color="">
                            {item.cost} USD
                          </Typography>
                          <Typography>
                          Mã SP:  {item.code}
                         
                          </Typography>
                          <Typography><NavLink to={`/Details`}>Details product</NavLink></Typography>
                          <Rating
                            name="simple-controlled"
                            value={this.state.value}
                            disabled
                          />
                        </CardContent>
                      </Card></Item>
                  </Grid>))}

              </Grid>

            </Box>


          </div>}
        <div className="pagination" style={{ paddingTop: 25, justifyContent: 'center' }}>    <Pagination /> </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  const { isLoading, listData } = state.items
  console.log('mapStateToProps:', listData)
  return {
    isLoading, listData
  }
}

const mapDispatchToProps = (dispatch) => ({
  getData: (payload) => {
    dispatch(getData.request(payload))
  },

})

export default connect(mapStateToProps, mapDispatchToProps)(ListGiay)


