import React from 'react'
import {
    Pagination
} from '@mui/material';
import { connect } from 'react-redux'
import { getData } from '../../actions/ItemAction'
class PaginationCustom extends React.Component {
    changePage = (e, page) => {
        this.props.getData({
            activePage: page,
            textSearch: this.props.textSearch
        })
        
    }
    render() {
        const { activePage, totalPage } = this.props
        return (
            <div >
                <Pagination page={activePage} count={totalPage} onChange={this.changePage} size="large" />
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    activePage: state.items.activePage,
    totalPage: state.items.totalPage,
    textSearch: state.items.textSearch
})

const mapDispatchToProps = (dispatch) => ({
    getData: (payload) => dispatch(getData.request(payload)),
})

export default connect(mapStateToProps, mapDispatchToProps)(PaginationCustom)
