import * as React from 'react';
import { styled } from '@mui/material/styles';
import { Box, Card, CardHeader, CardMedia, CardContent, Collapse, Typography, Paper, Grid, Rating } from '@mui/material'
import '../common/common.css'
import Pagination from './Pagination';
import { getData } from '../../actions/ItemAction';
import { connect } from 'react-redux';
import { NavLink } from 'react-router-dom'
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === 'dark' ? '#1A2027' : '#fff',
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.secondary,
}));

// 


class ListHome extends React.Component {
  state = {
    value: 2,

  }
  componentDidMount() {
    this.props.getData({ textSearch: '', activePage: 1 })

  }
  render() {
    const { isLoading, listData } = this.props
    console.log('listData', listData)


    return (
      <div>

        {isLoading ?
          <img src='https://cdn.dribbble.com/users/902865/screenshots/4814970/loading-opaque.gif' />
          :
          <div className='product-containers'>
            <Box sx={{ flexGrow: 1 }}>
              <Grid container spacing={3}>
                {listData.map((item, idx) => (
                  <Grid item xs={3} key={idx}>
                    <Item>
                      <Card sx={{ maxWidth: 345 }}>
                        <CardHeader

                          // action={
                          //     <IconButton aria-label="settings">
                          //         <MoreVertIcon />
                          //     </IconButton>
                          // }
                          title={item.name}
                        // subheader="September 14, 2016"
                        />
                        <CardMedia
                          component="img"
                          height="200"
                          image={item.image}
                          alt="Paella dish"
                        />
                        <CardContent>
                          <Typography variant="body2" color="">
                            {item.cost} USD
                          </Typography>
                          <Typography>
                            Mã SP:  {item.code}
                          </Typography>
                          <Typography><NavLink to={`/Details`}>Details product</NavLink></Typography>

                          <Rating
                            name="simple-controlled"
                            value={this.state.value}
                            // onChange={(event, newValue) => {
                            // this.setState({
                            //   value:newValue
                            // })
                            // }}
                            disabled
                          />
                        </CardContent>
                      </Card>
                    </Item>
                  </Grid>))}

              </Grid>

            </Box>


          </div>}
        <div className="pagination" style={{ paddingTop: 25, justifyContent: 'center' }}>    <Pagination /> </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  const { isLoading, listData } = state.items
  console.log('mapStateToProps:', listData)
  return {
    isLoading, listData
  }
}

const mapDispatchToProps = (dispatch) => ({
  getData: (payload) => {
    dispatch(getData.request(payload))
  },

})

export default connect(mapStateToProps, mapDispatchToProps)(ListHome)
