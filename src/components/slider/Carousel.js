import React from 'react';
import {
  MDBCarousel,
  MDBCarouselInner,
  MDBCarouselItem,
  MDBCarouselElement,
  MDBCarouselCaption,
} from 'mdb-react-ui-kit';
import 'mdb-react-ui-kit/dist/css/mdb.min.css'
import '../common/common.css'

export default function Carousel() {
  return (
    <div  >
      <MDBCarousel showIndicators showControls >
        <MDBCarouselInner>
          <MDBCarouselItem className='active'>
            <MDBCarouselElement src='https://bizweb.dktcdn.net/100/347/923/themes/742041/assets/slider_1.png?1642786252836' alt='...' className='item-carousel' />
            <MDBCarouselCaption className='text-carousel'>

            </MDBCarouselCaption>
          </MDBCarouselItem>
          <MDBCarouselItem>
            <MDBCarouselElement src='https://bizweb.dktcdn.net/100/347/923/themes/742041/assets/slider_2.png?1642786252836' alt='...' className='item-carousel' />
            <MDBCarouselCaption className='text-carousel'>

            </MDBCarouselCaption>
          </MDBCarouselItem>
          <MDBCarouselItem>
            <MDBCarouselElement src='	https://bizweb.dktcdn.net/100/347/923/themes/742041/assets/slider_3.png?1642786252836' alt='...' className='item-carousel' />
            <MDBCarouselCaption className='text-carousel'>

            </MDBCarouselCaption>
          </MDBCarouselItem>

        </MDBCarouselInner>
      </MDBCarousel>
    </div>
  );
}