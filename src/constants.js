const DOMAIN = 'http://localhost:3001',
    HTTP_METHOD = {
        GET: 'GET',
        POST: 'POST',
        PUT: 'PUT',
        DELETE: 'DELETE'
    },
    HEADERS = { "Content-Type": "Application/json",
                "Authorization": localStorage.getItem('token')
 },
    ACTION_TYPE = {
        ITEM: {
            GET_LIST_REQUEST: 'GET_ITEMS_REQUEST',
            GET_LIST_SUCCESS: 'GET_ITEMS_SUCCESS',
            GET_LIST_FAILURE: 'GET_ITEMS_FAILURE',
            CREATE_REQUEST: 'CREATE_ITEM_REQUEST',
            CREATE_SUCCESS: 'CREATE_ITEM_SUCCESS',
            CREATE_FAILURE: 'CREATE_ITEM_FAILURE',
            UPDATE_REQUEST: 'UPDATE_ITEM_REQUEST',
            UPDATE_SUCCESS: 'UPDATE_ITEM_SUCCESS',
            UPDATE_FAILURE: 'UPDATE_ITEM_FAILURE',
            DELETE_REQUEST: 'DELETE_ITEM_REQUEST',
            DELETE_SUCCESS: 'DELETE_ITEM_SUCCESS',
            DELETE_FAILURE: 'DELETE_ITEM_FAILURE',
            SELECT: 'SELECT_ITEM',
            GET_CATAGORIES_REQUEST:"GET_CATAGORIES_REQUEST",
            GET_CATAGORIES_SUCCESS:"GET_CATAGORIES_SUCCESS",
            GET_CATAGORIES_FAIL:"GET_CATAGORIES_FAIL",
            LOGIN_REQUEST:" LOGIN_REQUEST",
            LOGIN_SUCCESS:" LOGIN_SUCCESS",
            LOGIN_FAIL:"LOGIN_FAIL",
            LOG_OUT:"LOGOUT"
        }
    },
    LIMIT = 8

export {
    DOMAIN, HEADERS, HTTP_METHOD, ACTION_TYPE, LIMIT
}