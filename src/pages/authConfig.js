import React from 'react'
export let AuthContext = React.createContext();
function useAuth() {
    return React.useContext(AuthContext);
}

export default useAuth;