import { ACTION_TYPE } from '../constants'
let user = JSON.parse(localStorage.getItem('user'));
const { ITEM } = ACTION_TYPE;
const DEFAUT_STATE = {
    isloading: false,
    errors: null,
    listData: [],
    selected: null,
    keySearch: "",
    totalPage: 20,
    activePage: 1,
    listDetail:{},
    listFilter:[]    
}

export default (state = DEFAUT_STATE, action) => {
    switch (action.type) {
        case ITEM.GET_LIST_REQUEST:
        // case ITEM.CREATE_REQUEST:
        // case ITEM.UPDATE_REQUEST:
        // case ITEM.DELETE_REQUEST:
        // case ITEM.GET_CATAGORIES_REQUEST:
        // case ITEM.GET_DETAIL_REQUEST:
            return {
                ...state,
                isloading: true
            }
        case ITEM.GET_LIST_SUCCESS:
            const { listData, activePage, totalPage,keySearch,listFilter } = action.payload
            return{
                ...state,
                isloading: false,
                listData,
                activePage,
                totalPage,
                keySearch,
                errors:null
            }
        // case ITEM.CREATE_SUCCESS:
        // case ITEM.UPDATE_SUCCESS:
        // case ITEM.DELETE_SUCCESS:
        //     return {
        //         ...state,
        //         isloading: false,
        //         errors: null
        //     }
        case ITEM.GET_LIST_FAIL:
        // case ITEM.CREATE_FAIL:
        // case ITEM.UPDATE_FAIL:
        // case ITEM.DELETE_FAIL:
        // case ITEM.CREATE_FAIL:
        // case ITEM.GET_CATAGORIES_FAIL:
            return{
                ...state,
                isloading: false,
                errors: action.payload
            }
        case ITEM.SELECT:
            return{
                ...state,
                selected: action.payload
            }
        case ITEM.GET_DETAIL_SUCCESS:
            return {
                ...state,
                isloading: false,
                listDetail: action.payload
            }
        case ITEM.GET_CATAGORIES_SUCCESS:
            return {
                ...state,
                isloading: false,
                listFilter,
                activePage,
                totalPage,
                keySearch,
                errors:null
            }
        default:
            return state;
    }

}