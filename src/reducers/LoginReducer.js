import { ACTION_TYPE } from '../constants'
let user = JSON.parse(localStorage.getItem('user'));
const { ITEM } = ACTION_TYPE;
const DEFAUT_STATE ={
    isloading: false,
    errors: false,
    errorMsg:null,
    data:null
}
export default (state = DEFAUT_STATE, action)=>{
    switch(action.type){
        case ITEM.LOGIN_REQUEST:
            return{
                ...state,
                isloading:true,
                data:null
            }
        case ITEM.LOGIN_SUCCESS:
            const data = action.payload
            return{
                ...state,
                isloading :false,
                data
            }
        case ITEM.LOGIN_FAIL:
            return{
                ...state,
                errors:true,
                errorMsg: action.payload,
                data:null
            }
        default:
            return state
    }
}