import { combineReducers } from "redux";
import ItemReducer from "./ItemReducer";
import LoginReducer from "./LoginReducer";
export default combineReducers({
    items: ItemReducer,
    signin: LoginReducer
})