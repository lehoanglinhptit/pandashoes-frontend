import * as React from "react";
import {
  Routes, Route, Link, useNavigate, useLocation, Navigate, Outlet,
} from "react-router-dom";
import { fakeAuthProvider } from "../auth";
import LoginPage from "../components/acount_login_register/Login"
import useAuth, { AuthContext } from "../pages/authConfig";
import PublicPage from '../components/common/Home'
import Vans from "../components/list_sp/Vans";
import AllItem from "../components/list_sp/AllItem";
import Converse from "../components/list_sp/Converse";
import Details from "../components/details/Details";
import ShoppingCart from "../components/common/ShoppingCart";
import Register from "../components/acount_login_register/Register";
export default function App() {
  return (
    <AuthProvider>
      <Routes>

        <Route path="/" element={<PublicPage />} />
        <Route path="/Login" element={<LoginPage />} />
        <Route path="/Vans" element={<Vans />} />
        <Route path="/Converse" element={<Converse />} />
        <Route path="/ALLPRODUCTS" element={<AllItem />} />
        <Route path="/Details" element={<Details />} />
        <Route path="/Register" element={<Register />} />
        <Route path="/Shopping-cart" element={<RequireAuth>
              <ShoppingCart/>
              
            </RequireAuth>} />
        <Route
          path="/protected"
          element={
            <RequireAuth>
              <PublicPage />
              
            </RequireAuth>
          }
        />

      </Routes>
    </AuthProvider>
  );
}

function AuthProvider({ children }) {
  let [user, setUser] = React.useState(null);

  let signin = (newUser, callback) => {
    return fakeAuthProvider.signin(() => {
      setUser(newUser);
      callback();
    });
  };

  let signout = (callback) => {
    return fakeAuthProvider.signout(() => {
      setUser(null);
      callback();
    });
  };

  let value = { user, signin, signout };

  return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>;
}


function RequireAuth({ children }) {
  let auth = localStorage.getItem('token')
  let location = useLocation();

  if (!auth) {
    // Redirect them to the /login page, but save the current location they were
    // trying to go to when they were redirected. This allows us to send them
    // along to that page after they login, which is a nicer user experience
    // than dropping them off on the home page.
    return <Navigate to="/Login" state={{ from: location }} replace />;
  }

  return children;
}
// function PublicPage() {
//   return <h3>Public</h3>;
// }

// function ProtectedPage() {
//   return <h3>Protected</h3>;
// }

// function Layout() {
//   return (
//     <div>
//       <AuthStatus />

//       <ul>
//         <li>
//           <Link to="/">Public Page</Link>
//         </li>
//         <li>
//           <Link to="/protected">Protected Page</Link>
//         </li>
//       </ul>

//       <Outlet />
//     </div>
//   );
// }
// function AuthStatus() {
//   let auth = useAuth();
//   let navigate = useNavigate();

//   if (!auth.user) {
//     return <p>You are not logged in.</p>;
//   }

//   return (
//     <p>
//       Welcome {auth.user}!{" "}
//       <button
//         onClick={() => {
//           auth.signout(() => navigate("/"));
//         }}
//       >
//         Sign out
//       </button>
//     </p>
//   );
// }