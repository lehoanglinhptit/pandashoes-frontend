import { takeLatest, put, select } from 'redux-saga/effects'
import { ACTION_TYPE, LIMIT } from '../constants'
import * as actions from '../actions/ItemAction'
import * as callAPI from '../apis/ItemAPI'
import { Navigate } from "react-router-dom"
const { ITEM } = ACTION_TYPE
function* getListDataSaga({ payload }) {

    try {
        const response = yield callAPI.getData(payload.textSearch, payload.activePage);
        const allData = yield callAPI.getAll(payload.textSearch);
        const payloadSuccess = {
            listData: response,
            activePage: payload.activePage,
            totalPage: Math.ceil(allData.length / LIMIT),
            textSearch: payload.textSearch
        }
        yield put(actions.getData.success(payloadSuccess)) // luồng về
    } catch (error) {
        yield put(actions.getData.failure({ error: error.message }))
    }
}
function* loginSaga({ payload }) {

    try {
        console.log(payload)
        const response = yield callAPI.Login(payload)
        if (response.token) {
            localStorage.setItem("token", response.token)
            yield put(actions.Login.success("Login Success"))
        } else {
            yield put(actions.Login.failure({ error: response.msg }))
        }

        // yield window.location.href='/'
    } catch (error) {
        yield put(actions.Login.failure({ error: error.message }))
    }
}
export default [
    takeLatest(ITEM.GET_LIST_REQUEST, getListDataSaga),
    takeLatest(ITEM.LOGIN_REQUEST, loginSaga)
]